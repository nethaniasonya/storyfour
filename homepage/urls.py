from django.contrib import admin
from django.urls import path
from django.conf.urls import url
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.index, name='index'),
    path('about/', views.about, name='about'),
    path('contact/', views.contact, name='contact'),
    path('login/',views.login_page, name='login'),
    path('logout/', views.logout_page, name='logout')
]