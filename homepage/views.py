
from django.shortcuts import render, redirect, HttpResponse
from django.contrib.auth import login, authenticate,logout

# Create your views here.

def index(request):
    return render(request, 'index.html')

def about(request):
    return render(request, 'about.html')

def contact(request):
    return render(request, 'contact.html')

response = {}

def login_page(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request,user)
            response['username'] = username
            return redirect("/login/")
        else:
            return HttpResponse("invalid username or password")
    else:
        return render(request, 'account.html', response)

def logout_page(request):
    request.session.flush()
    logout(request)
    return redirect("/login/")